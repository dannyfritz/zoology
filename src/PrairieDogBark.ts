import { Sound, Vector2 } from "nuthatch";
import { Context } from "./Context";
import { Difficulty, IMinigame, Result } from "./IMinigame";
import { Sprite } from "./Sprite";
import dogImageUrl from "../assets/images/prairiedog-bark.png";
import hawkImageUrl from "../assets/images/hawk.png";
import bgUrl from "../assets/images/prairiedog-bg.png";
import barkUrl from "../assets/audio/prairiedog.ogg";
import cawUrl from "../assets/audio/hawk.ogg";
import musicUrl from "../assets/audio/Norrin_Radd_-_09_-_Quantum_Uncertainty.mp3";
import { IState } from "./IState";
const { Lose, Pending, Win } = Result;

const HAWK_FLIGHT_RATIO = 1 / 8;
const BARK_DURATION = 0.25;
const BG_IMAGE = new Image();
BG_IMAGE.src = bgUrl;
const DOG_IMAGE = new Image();
DOG_IMAGE.src = dogImageUrl;
const HAWK_IMAGE = new Image();
HAWK_IMAGE.src = hawkImageUrl;

enum DogState {
  Guard,
  Bark,
  Tired,
  Triumphant,
}

enum HawkState {
  Waiting,
  Flying,
  Done,
}

interface IActions {
  bark: boolean;
}

function interpolate(t: number, start: number, end: number): number {
  return t * end + (1 - t) * start;
}

/**
 * PrairieDogBark is a game involving a big head of a prairie dog.
 * If you bark too soon, you get a dry mouth and your friend gets eaten
 * If you bark too late, your friend gets eaten
 * If you bark at the right time, your friend survives
 * @param context
 * @param difficulty
 * @param duration
 */
export class PrairieDogBark implements IMinigame, IState {
  public result: Result = Pending;
  private context: Context;
  private difficulty: Difficulty;
  private timer: number;
  private duration: number;
  private bark: Promise<Sound>;
  private caw: Promise<Sound>;
  private dogSprite: Sprite;
  private hawkSprite: Sprite;
  private dogState: DogState;
  private barkDuration: number = BARK_DURATION;
  private hawkState: HawkState;
  private hawkT: number;
  private hawkStart: Vector2;
  private hawkEnd: Vector2;
  private hawkPos: Vector2;
  private hawkScale: number;
  private hawkScaleStart: number;
  private hawkScaleEnd: number;
  private hawkDuration: number;
  private music: Promise<Sound>;
  private musicInstance: AudioBufferSourceNode;
  private willWin: Result;
  private actions: IActions;
  constructor(context: Context, difficulty: Difficulty) {
    this.context = context;
    this.difficulty = difficulty;
    this.willWin = Pending;
    this.bark = context.audio.sfx.load(barkUrl);
    this.caw = context.audio.sfx.load(cawUrl);
    this.music = context.audio.music.load(musicUrl);
    this.music.then((sound) => {
      this.musicInstance = this.context.audio.music.play(sound);
    })
    this.dogSprite = new Sprite(DOG_IMAGE, {x: 48, y: 48});
    this.dogState = DogState.Guard;
    this.hawkSprite = new Sprite(HAWK_IMAGE, {x: 48, y: 48});
    this.hawkState = HawkState.Waiting;
    this.hawkStart = { x: 0, y: 0 };
    this.hawkEnd = { x: 100, y: 100 };
    this.hawkPos = this.hawkStart;
    this.hawkScaleStart = 0;
    this.hawkScaleEnd = 3;
    this.hawkScale = this.hawkScaleStart;
    switch (difficulty) {
      case Difficulty.Easy:
        this.duration = 5;
        break;
      case Difficulty.Moderate:
        this.duration = 4;
        break;
      case Difficulty.Hard:
        this.duration = 3;
        break;
      default:
        throw new Error("Invalid Difficulty");
    }
    this.hawkT = this.duration * Math.random();
    this.hawkDuration = this.duration * HAWK_FLIGHT_RATIO;
    this.timer = this.duration;
    this.actions = {
      bark: false,
    };
  }
  public destroy(): void {
    this.musicInstance.stop();
  }
  public input() {
    this.actions = {
      bark: this.context.keyboard.isKeyDown("z"),
    };
  }
  public update(dt: number) {
    this.timer -= dt;
    if (this.timer <= 0) {
      if (this.willWin === Win) {
        this.result = Win;
      } else {
        this.result = Lose;
      }
    }
    switch (this.hawkState) {
      case HawkState.Waiting:
        if (this.timer <= this.hawkT) {
          this.hawkState = HawkState.Flying;
          this.caw.then((sound) => this.context.audio.sfx.play(sound));
          this.hawkT = 0;
        }
        break;
      case HawkState.Flying:
        this.hawkT += dt;
        const t = this.hawkT / this.hawkDuration;
        this.hawkPos = {
          x: interpolate(t, this.hawkStart.x, this.hawkEnd.x),
          y: interpolate(t, this.hawkStart.y, this.hawkEnd.y),
        };
        this.hawkScale = interpolate(t, this.hawkScaleStart, this.hawkScaleEnd);
        if (this.hawkT >= this.hawkDuration) {
          this.hawkState = HawkState.Done;
        }
        break;
      case HawkState.Done:
        break;
    }
    switch (this.dogState) {
      case DogState.Guard:
        if (this.actions.bark) {
          if (this.hawkState === HawkState.Flying) {
            this.willWin = Win;
          } else {
            this.willWin = Lose;
          }
          this.dogSprite.setCell(1);
          this.dogState = DogState.Bark;
          this.bark.then((sound) => this.context.audio.sfx.play(sound));
        }
        break;
      case DogState.Bark:
        this.barkDuration -= dt;
        if (this.barkDuration <= 0) {
          if (this.willWin === Win) {
            this.dogState = DogState.Triumphant;
            this.dogSprite.setCell(3); //TODO
          } else {
            this.dogState = DogState.Tired;
            this.dogSprite.setCell(2); //TODO
          }
        }
        break;
    }
  }
  public draw(): void {
    const graphics = this.context.graphics;
    const matrices = this.context.matrices;
    graphics.clear({r: 0, g: 0, b: 0, a: 1});
    matrices.save();
    graphics.setTransformMatrix(matrices.current);
    graphics.image(BG_IMAGE);
    matrices.restore();
    switch (this.hawkState) {
      case HawkState.Flying:
        if (this.hawkSprite.image.complete) {
          matrices.save();
          matrices.translate(this.hawkPos);
          matrices.scale({ x: this.hawkScale, y: this.hawkScale });
          graphics.setTransformMatrix(matrices.current);
          graphics.sprite(this.hawkSprite.image, this.hawkSprite.position, this.hawkSprite.size);
          matrices.restore();
        }
        break;
    }
    if (this.dogSprite.image.complete) {
      matrices.save();
      matrices.translate({ x: 80, y: 0 });
      matrices.scale({ x: 3, y: 3 });
      graphics.setTransformMatrix(matrices.current);
      graphics.sprite(this.dogSprite.image, this.dogSprite.position, this.dogSprite.size);
      matrices.restore();
    }
  }
}
