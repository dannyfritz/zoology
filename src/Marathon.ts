import { None, Option, OptionState, Some } from "nuthatch";
import { Context } from "./Context";
import { Difficulty, IMinigame, Result } from "./IMinigame";
import { IState } from "./IState";
import { PrairieDogBark } from "./PrairieDogBark";
const { Lose, Pending, Win } = Result;
const { Easy } = Difficulty;

/**
 * Marathon manages a string of minigames in sucession.
 * The user is presented with a minigame
 *  If they win, they get another
 *  If they lose, they lose a life
 *  If they are out of lives, they get a scorecard
 * @param context
 * @param lives
 */
export class Marathon implements IState {
  private context: Context;
  private score: number;
  private lives: number;
  private minigame: Option<IMinigame & IState> = None();
  private cooldown: number = 0;
  private actions: {};
  constructor(context: Context, lives: number) {
    this.context = context;
    this.lives = lives;
    this.score = 0;
    this.actions = {};
  }
  public input() {
    if (this.minigame.state === OptionState.Some) {
      this.minigame.value.input();
    }
    this.actions = {};
  }
  public update(dt: number) {
    this.cooldown -= dt;
    if (this.minigame.state === OptionState.Some) {
      const minigame = this.minigame.value;
      if (minigame.result === Win) {
        console.log("won")
        this.score += 1;
      } else if (minigame.result === Lose) {
        console.log("lost")
        this.lives -= 1;
      }
      if (minigame.result !== Pending) {
        minigame.destroy();
        this.minigame = None();
        this.cooldown = 1;
      } else {
        minigame.update(dt);
      }
    }
    if (this.lives > 0 && this.cooldown <= 0
        && this.minigame.state === OptionState.None) {
      this.minigame = Some(new PrairieDogBark(this.context, Easy));
    }
  }
  public draw(): void {
    this.context.graphics.clear({r: 0, g: 0, b: 0, a: 1});
    if (this.minigame.state === OptionState.Some) {
      this.minigame.value.draw();
    }
    if (this.lives === 0) {
      // console.log(`Score: ${this.score}`);
    }
  }
}
