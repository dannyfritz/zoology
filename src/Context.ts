import {
  Audio,
  Graphics,
  Keyboard,
  Matrix, MatrixState,
} from "nuthatch";

export class Context {
  public graphics: Graphics;
  public keyboard: Keyboard;
  public matrices: MatrixState;
  public audio: {
    music: Audio,
    sfx: Audio,
  };
  constructor() {
    this.audio = {
      music: new Audio(),
      sfx: new Audio(),
    };
    this.audio.sfx.setVolume(0.5);
    this.audio.music.setVolume(0.2);
    const container = document.querySelector("main") as HTMLElement;
    container.innerHTML = "";
    const size = {x: 240, y: 160};
    this.graphics = new Graphics(container, size);
    this.keyboard = new Keyboard();
    this.matrices = new MatrixState(Matrix.orthographic(size));
    this.matrices.save();
  }
}
