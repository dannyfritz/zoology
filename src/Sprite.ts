import { Vector2 } from "nuthatch";

export class Sprite {
  public cellNum: number;
  public position: Vector2;
  public size: Vector2;
  public image: HTMLImageElement;
  constructor(image: HTMLImageElement, cellSize: Vector2) {
    this.cellNum = 0;
    this.image = image;
    this.size = cellSize;
    this.position = {x: 0, y: 0};
  }
  public setCell(cellNum: number) {
    this.cellNum = cellNum;
    this.position = {
      x: this.size.x * cellNum,
      y: 0,
    };
  }
}
