import { FixedRuntime } from "nuthatch";
import { Context } from "./Context";
import { IState } from "./IState";
import { Marathon } from "./Marathon";

document.addEventListener("keydown", (e) => {
    if (e.code === "ArrowUp" || e.code === "ArrowDown"
        || e.code === "ArrowLeft" || e.code === "ArrowRight"
        || e.code === "KeyZ" || e.code === "KeyX") {
      e.preventDefault();
    }
  },
);
const context = new Context();
const state: IState = new Marathon(context, 300);
const runtime = new FixedRuntime();
runtime.input = () => state.input();
runtime.update = (dt: number) => state.update(dt);
runtime.draw = () => state.draw();
runtime.start();
