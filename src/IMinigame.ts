export enum Result {
  Lose, Pending, Win,
}

export enum Difficulty {
  Easy, Moderate, Hard,
}

export interface IMinigame {
  result: Result;
  destroy: () => void
}
