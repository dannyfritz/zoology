export interface IState {
  input: () => void;
  update: (dt: number) => void;
  draw: () => void;
}
